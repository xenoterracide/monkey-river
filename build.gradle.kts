
import org.gradle.api.credentials.AwsCredentials
import org.gradle.kotlin.dsl.maven
import org.gradle.kotlin.dsl.repositories
import org.gradle.internal.impldep.junit.runner.Version.id
import org.gradle.kotlin.dsl.creating
import org.gradle.kotlin.dsl.support.illegalElementType
import org.gradle.model.internal.core.TypeCompatibilityModelProjectionSupport.description
import org.gradle.plugins.ide.idea.model.IdeaModel
import org.gradle.plugins.ide.idea.model.IdeaProject


plugins {
    `java-library`
}

repositories {
    mavenCentral()
}

group = "com.xenoterracide"
version = "0.1.0-SNAPSHOT"


dependencies {
    implementation("org.springframework.boot:spring-boot-starter:1.5.9.RELEASE")
    testImplementation("junit", "junit", "4.12")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
