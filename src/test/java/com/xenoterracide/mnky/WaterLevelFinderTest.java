package com.xenoterracide.mnky;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class WaterLevelFinderTest {

    @Test
    public void findLevelTwoHops() {
        WaterLevelFinder finder = new WaterLevelFinder( 2 );
        assertThat( finder.findLevel( -1, 2, 5, 3, -1 ), is( 3 ) );
    }

    @Test
    public void findLevelThreeHops() {
        WaterLevelFinder finder = new WaterLevelFinder( 3 );
        assertThat( finder.findLevel( -1, -1, 2, 5, 3, -1 ), is( 5 ) );
    }

    @Test
    public void findLevelFourHops() {
        WaterLevelFinder finder = new WaterLevelFinder( 2 );
        assertThat( finder.findLevel( -1, 2, -1, 3, 4, -1 ), is( 4) );
    }

    @Test
    public void noMatch() {
        WaterLevelFinder finder = new WaterLevelFinder( 2 );
        assertThat( finder.findLevel( -1, 2, 5, -1, -1 ), is( -1 ) );
    }
}
