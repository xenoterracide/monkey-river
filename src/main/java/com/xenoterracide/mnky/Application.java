package com.xenoterracide.mnky;

import java.util.List;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements ApplicationRunner {

    public static void main( String... args ) {
        SpringApplication.run( Application.class, args );
    }

    /**
     * bad API doesn't use DDD, and is unreadable, but contract that's given
     * @param a array of stones where they appear at what water level
     * @param d max amount of space a monkey can hop
     * @return water level monkey can cross at
     */
    static int solution( int [] a, int d ) {
        return new WaterLevelFinder( d ).findLevel( a );
    }

    @Override
    public void run( final ApplicationArguments args ) throws Exception {
        int[] levels = args.getOptionValues("stones")
                .stream()
                .mapToInt( Integer::parseInt )
                .toArray();

        List<String> nonOptionArgs = args.getNonOptionArgs();
        for ( String nonOptionArg : nonOptionArgs ) {
            System.out.println(solution( levels, Integer.parseInt( nonOptionArg ) ) );
        }
    }
}
