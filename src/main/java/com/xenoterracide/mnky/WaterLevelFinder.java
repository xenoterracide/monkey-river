package com.xenoterracide.mnky;

class WaterLevelFinder {


    private static final int NOSTONE = -1;
    private static final int NOMATCH = -1;
    private final int jumpSize;

    WaterLevelFinder( final int maxHops ) {
        this.jumpSize = maxHops;
    }

    int findLevel( int... stones ) {

        if ( stones.length >= jumpSize ) {
            int hop = 0;
            for ( int i = 0; i < stones.length; i++ ) {
                int stone = stones[i];

                if ( hop > jumpSize ) {
                    return NOMATCH;
                }
                if ( stone <= NOSTONE ) {
                    hop = hop+1;
                }
                else {
                    hop = 0;
                }
                if( i + jumpSize +1 > stones.length ) {
                    return stone;
                }

            }
        }
        return NOMATCH;
    }

}
